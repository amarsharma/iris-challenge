import cv2
from fastai.vision import *

learn = load_learner('./final')


# Formats and write condition on image
def write_conditions(cv2image, preds):
	condition_hash = {"cloudy": "Clouds Visible", "sunny": "Sun Visible"}
	font = cv2.FONT_HERSHEY_SIMPLEX
	font_size = 0.5
	font_thickness = 1
	text_num = 0
	x = cv2image.shape[1] - 300
	overlay = cv2image.copy()
	conditions = ['Conditions: ', " "]
	for index, pred in enumerate(preds):
		if pred.strip() == "":
			continue
		if condition_hash.get(pred) is not None:
			conditions.append(str(index) + ". " + condition_hash[pred])
		else:
			conditions.append(str(index) + ". " + pred.capitalize())
	cv2.rectangle(overlay, (x - 10, int(overlay.shape[0] / 5) - 20),
	              (x + 150, int(overlay.shape[0] / 5 + len(conditions) * 18)),
	              (0, 0, 0), -1)
	opacity = 0.5
	cv2.addWeighted(overlay, opacity, cv2image, 1 - opacity, 0, cv2image)
	for line in conditions:
		text_size = cv2.getTextSize(line, font, font_size, font_thickness)[0]
		gap = text_size[1] + 5
		y = int((cv2image.shape[0] + text_size[1]) / 5) + text_num * gap
		cv2.putText(cv2image, line, (x, y), font,
		            font_size,
		            (0, 0, 255),
		            font_thickness,
		            lineType=cv2.LINE_AA)
		text_num += 1
	return cv2image


# predict by image name supplied
def get_condition(image_name):
	img = open_image(image_name)
	preds, y, losses = learn.predict(img, thresh=0.4)
	return str(preds).split(";")


def _main():
	for i in range(1, 31):
		img_file_name = 'images/image' + str(i) + '.png'
		im = cv2.imread(img_file_name)
		conditions = get_condition(img_file_name)
		write_conditions(im, conditions)


if __name__ == '__main__':
	_main()
