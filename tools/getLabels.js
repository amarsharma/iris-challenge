var admin = require('firebase-admin');
var serviceAccount = require('FIREBASE SERVICE ACCOUNT JSON');
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
});
var db = admin.firestore();

csv = 'file, label\n';

db.collection('iris')
  .get()
  .then(snapshot => {
    snapshot.forEach(doc => {
      if (doc.data().label && doc.data().label.length > 0) {
        console.log(doc.id, '=>', doc.data());
        csv += doc.id + ', ' + doc.data().label.join(' ') + '\n';
      }
    });
    var fs = require('fs');

    fs.writeFile('train_db.csv', csv, function(err) {
      if (err) throw err;
      console.log('Saved!');
    });
  })
  .catch(err => {
    console.log('Error getting documents', err);
  });
