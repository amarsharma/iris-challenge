import cv2

RADIUS = 101


# Getting Sun using minmax
def get_light_source(cv2image):
	image = cv2image.copy()
	gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
	gray = cv2.GaussianBlur(gray, (RADIUS, RADIUS), 0)
	(minVal, maxVal, minLoc, maxLoc) = cv2.minMaxLoc(gray)
	cv2.circle(image, maxLoc, RADIUS, (255, 0, 0), 2)
	return image
