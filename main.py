import cv2
import numpy as np

from detect_condition import get_condition, write_conditions
from detect_horizon import get_horizon
from detect_sun import get_light_source


# Resize Image
def resize_image(img):
	scale_percent = 70  # percent of original size
	width = int(img.shape[1] * scale_percent / 100)
	height = int(img.shape[0] * scale_percent / 100)
	dim = (width, height)
	# resize image
	resized = cv2.resize(img, dim, interpolation=cv2.INTER_AREA)
	return resized


# Put title in Image
def title_img(cv2image, title):
	y, x, _ = cv2image.shape
	x -= 300
	y = 50
	font = cv2.FONT_HERSHEY_SIMPLEX
	font_size = 0.8
	font_thickness = 1
	cv2.putText(cv2image, title, (x, y), font,
	            font_size,
	            (0, 255, 0),
	            font_thickness,
	            lineType=cv2.LINE_AA)


def main():
	for file_num in range(1, 31):
		img_file_name = 'images/image' + str(file_num) + '.png'
		cv2image = cv2.imread(img_file_name)
		# Gets all prediction of conditions in Image
		preds = get_condition(img_file_name)
		# Format and write the predictions on Image
		conditions_img = write_conditions(cv2image.copy(), preds)
		# Find and draw image of horizon on image
		horizon_img = get_horizon(cv2image.copy())
		light_src_img = None
		if "sunny" in preds:
			# Circle sun from image
			light_src_img = get_light_source(cv2image.copy())
		if conditions_img is None:
			conditions_img = cv2image.copy()
		if horizon_img is None:
			horizon_img = cv2image.copy()
		if light_src_img is None:
			light_src_img = cv2image.copy()
		title_img(conditions_img, "Conditions")
		title_img(horizon_img, "Horizon")
		title_img(light_src_img, "Sun")
		output = [cv2image.copy(), conditions_img, horizon_img, light_src_img]
		output = resize_image(np.hstack([np.vstack(output[:2]), np.vstack(output[2:])]))
		cv2.imwrite('output/solution_image' + str(file_num) + '.png', output)
	# cv2.imshow("Output ", output)
	# cv2.waitKey(0)
	# cv2.destroyAllWindows()


if __name__ == '__main__':
	main()
