import math

import cv2
import numpy as np
from sklearn.cluster import MiniBatchKMeans


def get_image_quant(image, cluster):
	(h, w) = image.shape[:2]
	# convert the image from the RGB color space to the L*a*b*
	# color space -- since we will be clustering using k-means
	# which is based on the euclidean distance, we'll use the
	# L*a*b* color space where the euclidean distance implies
	# perceptual meaning
	image = cv2.cvtColor(image, cv2.COLOR_BGR2LAB)
	# reshape the image into a feature vector so that k-means
	# can be applied
	image = image.reshape((image.shape[0] * image.shape[1], 3))
	# apply k-means using the specified number of clusters and
	# then create the quantized image based on the predictions
	clt = MiniBatchKMeans(n_clusters=cluster)
	labels = clt.fit_predict(image)
	quant = clt.cluster_centers_.astype("uint8")[labels]
	# reshape the feature vectors to images
	quant = quant.reshape((h, w, 3))
	# convert from L*a*b* to RGB
	quant = cv2.cvtColor(quant, cv2.COLOR_LAB2BGR)
	return quant


def plot_line(img, rho, theta, color=(255, 0, 0), thickness=3):
	a = math.cos(theta)
	b = math.sin(theta)
	x0 = a * rho
	y0 = b * rho
	pt1 = (int(x0 + 10000 * (-b)), int(y0 + 10000 * (a)))
	pt2 = (int(x0 - 10000 * (-b)), int(y0 - 10000 * (a)))
	cv2.line(img, pt1, pt2, color, thickness)


def get_slope(p1, p2):
	slope = (p2[1] - p1[1]) / (p2[0] - p1[0])
	return slope


def black_sky(thresh):
	h, w = np.shape(thresh)
	blacks = 0
	whites = 0
	for x in range(0, w - 1):
		for y in range(0, int(h / 5)):
			if thresh[y][x] == 0:
				blacks += 1
			else:
				whites += 1
	return blacks > whites


def resize_image(img):
	scale_percent = 60  # percent of original size
	width = int(img.shape[1] * scale_percent / 100)
	height = int(img.shape[0] * scale_percent / 100)
	dim = (width, height)
	# resize image
	resized = cv2.resize(img, dim, interpolation=cv2.INTER_AREA)
	return resized


def get_horizon(cv2image):
	img = cv2image.copy()
	output = cv2image.copy()
	# Divide image into two colors, to make a separation
	quant = get_image_quant(img, 2)
	gray = cv2.cvtColor(quant, cv2.COLOR_BGR2GRAY)
	(T, thresh) = cv2.threshold(gray, 100, 255, cv2.THRESH_BINARY)
	# To check if sky is dark
	is_black_sky = black_sky(thresh)
	thresh = cv2.adaptiveThreshold(gray, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 11, 2)
	dilated = cv2.dilate(thresh, np.ones((3, 3), dtype=np.uint8))
	window = 100
	h, w = np.shape(dilated)
	points = []

	for x in range(0, w - 1):
		skipped_black_sky = not is_black_sky
		skip_y_from = 0
		for y in range(20, h - 1):
			if thresh[y][x] != 255 and y >= skip_y_from:
				if not skipped_black_sky:
					skipped_black_sky = True
					skip_y_from = y + window
					continue
				points.append((x, y))
				break
	slopes = [get_slope(points[ind], points[ind + 30]) for ind in range(0, len(points) - 31)]
	mean_x = np.mean([p[0] for p in points])
	mean_y = np.median([p[1] for p in points])
	M = np.median(slopes)
	# Y = MX +C
	C = mean_y - mean_x * M
	if math.isnan(mean_x) or math.isnan(mean_y):
		print("No Horizon")
		return None
	cv2.line(output, (0, int(C)), (w - 1, int(M * (w - 1) + C)), [255, 0, 0], 3)
	return output


def _main():
	for i in range(1, 31):
		img_file_name = 'images/image' + str(i) + '.png'
		image = cv2.imread(img_file_name)
		output = get_horizon(image)
		if output is None:
			print("No Horizon found")
			continue
		output = resize_image(np.vstack([image, output]))
		cv2.imshow("Output ", output)
		cv2.waitKey(0)
		cv2.destroyAllWindows()


if __name__ == '__main__':
	_main()
