## Iris Automation Challenge

### Setup

This project contains solution to Iris Automation [public hiring challenge](https://drive.google.com/drive/folders/0B50bwdqRospTRU1PejNyMldLNnc).

To run the solution you'll need to install CUDA, fastai and opencv, you can install them using Anaconda (recommended)

To install fastai you can use this [guide](https://docs.fast.ai/install.html)

You can run the solution in conda env using `python main.py`

`main.py` requires an empty folder [output](./output) in root directory and [final](./final) directory with weights which is already committed in this repository.

### Output

Output of the program is already in repository for your convinience in [output](./output)


### Sample input
![Input image](./images/image29.png?raw=true "Input")

### Sample Output
![Output image](./output/solution_image29.png?raw=true "Input")


### Deep Learning resnet training

To run the Classification [trainer](https://gitlab.com/amarsharma/iris-challenge/blob/master/Classification%20training.ipynb) you'll need the all image files in [final](./final) which you can download iteratively from my public Firebase Storage [image1](https://firebasestorage.googleapis.com/v0/b/irischallenge/o/final%2Fimage1.jpg?alt=media) to [image3198](https://firebasestorage.googleapis.com/v0/b/irischallenge/o/final%2Fimage3198.jpg?alt=media)
The labels are stored in my firestore database, All labels have already been compiled and included all the labels in [train.csv](final/train.csv)

To create this dataset [`google_images_download.py`](tools/google_images_download.py) was used to download images from google and data was labelled by me and my friends using [Retool](https://amarsharma.retool.com/apps/Tagger)

#### Retool Screenshot for tagging images

![Retool SS](./retool_ss.PNG)

### Thank You!